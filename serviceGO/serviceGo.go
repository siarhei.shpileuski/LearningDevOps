package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/urfave/negroni"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Hello World!")
	r := mux.NewRouter()
	r.HandleFunc("/test/", testFunc1).Methods("GET")       //лист
	r.HandleFunc("/test/{key}", testFunc2).Methods("GET")  //1 элемент
	r.HandleFunc("/test/{key}", testFunc3).Methods("POST") // создание нового

	//r.HandleFunc("/test1/{id:[0-9]+}", testFunc2)

	n := negroni.New(negroni.HandlerFunc(Auth))
	n.UseHandler(r)

	//http.Handle("/", r)

	http.ListenAndServe(":8080", n)

}

func testFunc(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	//r.Method = "POST" // "GET" , "DELETE" , "PUT"
	// if r.Method == "GET" {
	// 	var b jsonSstruct
	// 	a, _ := json.Marshal(b)
	// 	fmt.Fprintf(w, string(a))
	// }

	// var b jsonSstruct

	// r.ParseForm()
	// d := r.FormValue("var1")
	// b.P1 = d
	// f := r.FormValue("var2")
	// b.P2 = f

	dec := json.NewDecoder(r.Body)
	var a structTest
	dec.Decode(&a)
	f, _ := json.Marshal(a)
	fmt.Fprintf(w, string(f))
}

func testFunc1(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Вывод списка.")

}

func testFunc2(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Вовод 1 элемента. \n")
	val := mux.Vars(r)
	fmt.Fprintf(w, "ID: "+val["key"])
}

func testFunc3(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Создание нового. \n")
	val := mux.Vars(r)
	fmt.Fprintf(w, "ID: "+val["key"])
}

//если 111 то работаем дальше
func Auth(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Println(r.URL.Path)
	if r.URL.Path == "/test/111" {
		next(w, r)
	} else {
		fmt.Fprintf(w, "Authorization error")
		return
	}

}

type jsonSstruct struct {
	P1 string `json:"p1"`
	P2 string `json:"p2" `
}

type structTest struct {
	P1 string `json:"p1"`
	P2 string `json:"p2" `
}
