package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/tidwall/gjson"

	"github.com/urfave/negroni"
)

type Struct1 struct {
	ArrStr []string
	Output string
}

var str Struct1

func init() {
	str.ArrStr = append(str.ArrStr, "USD")
	str.ArrStr = append(str.ArrStr, "EUR")
	str.ArrStr = append(str.ArrStr, "CZK")
	str.Output = "0"
}

func main() {

	fmt.Println("Start")
	r := mux.NewRouter()
	r.HandleFunc("/", testHendle).Methods("GET")
	r.HandleFunc("/", processHendle).Methods("POST")

	n := negroni.New(negroni.HandlerFunc(Auth))
	n.UseHandler(r)

	http.ListenAndServe(":8081", n)
}

func Auth(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Println("Auth")
	if r.Method == "GET" {
		t, _ := template.ParseFiles("mainPage.html")
		t.Execute(w, str)
	} else {
		date := r.FormValue("date")
		fmt.Println(date)
		d, _ := time.Parse("2006-01-02", date)
		dNow := time.Now()

		fmt.Println(time.Now().Date())

		if d.Year() > dNow.Year() || d.Month() > dNow.Month() || d.Day() > dNow.Day() {
			fmt.Println("Ошибка: неправильная дата.")
			str.Output = "Ошибка: неправильная дата."
			t, _ := template.ParseFiles("mainPage.html")
			t.Execute(w, str)
			return
		}

		summa := r.FormValue("summa")
		var validBarcode = regexp.MustCompile(`^[0-9]`)
		if validBarcode.MatchString(summa) != true {
			fmt.Println("Ошибка: присутствуют посторонние символы.")
			str.Output = "Ошибка: присутствуют посторонние символы."
			t, _ := template.ParseFiles("mainPage.html")
			t.Execute(w, str)
			return
		}

		firstValue := r.FormValue("firstValue")
		secondValue := r.FormValue("secondValue")
		if firstValue == secondValue {
			fmt.Println("Ошибка: валюта должна различаться.")
			str.Output = "Ошибка: валюта должна различаться."
			t, _ := template.ParseFiles("mainPage.html")
			t.Execute(w, str)
			return
		} else {
			next(w, r)
		}
	}
}

func testHendle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("testHendle")
	t, _ := template.ParseFiles("mainPage.html")
	t.Execute(w, str)
}

func processHendle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("processHendle")
	r.ParseForm()
	fmt.Println(r.Form)

	date := r.FormValue("date")
	firstValue := r.FormValue("firstValue")
	secondValue := r.FormValue("secondValue")
	summa := r.FormValue("summa")

	resp, _ := http.Get("http://api.fixer.io/" + date + "?base=" + firstValue)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	jsonStr := string(body)
	value := gjson.Get(jsonStr, "rates."+secondValue)

	s1, _ := strconv.ParseFloat(value.String(), 32)
	s2, _ := strconv.ParseFloat(summa, 32)
	rez := s1 * s2
	str.Output = strconv.FormatFloat(rez, 'f', -1, 32)

	t, _ := template.ParseFiles("mainPage.html")
	t.Execute(w, str)

}
